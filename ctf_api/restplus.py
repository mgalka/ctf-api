from flask_restplus import Api

api = Api(version='1.0', title='CTF API',
          description='Beacon CTF API')