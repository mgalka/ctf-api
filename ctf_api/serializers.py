from flask_restplus import fields
from ctf_api.restplus import api


position = api.model('Position update data',
    {
        'lat': fields.Float(description='lattitude'),
        'lon': fields.Float(description='longitude'),
        'beacons': fields.List(fields.String)
    }
)