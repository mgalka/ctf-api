from flask import request, jsonify
from flask_restplus import Resource, cors
from ctf_api.restplus import api
from ctf_api.data import beacons, position

ns = api.namespace('ctf/players', description='Players operations')


@ns.route('/')
class PlayerCollection(Resource):
    
    @cors.crossdomain(origin='*')
    def get(self):
        """
        Returns list of players.
        """
        result = []
        for user_data, pos_data in position.items():
            if pos_data['lat'] is None or pos_data['lon'] is None:
                continue 
            team, user = user_data
            player_item = {
                'team': team,
                'user': user
            }
            player_item.update(pos_data)
            result.append(player_item)
        return jsonify(result)